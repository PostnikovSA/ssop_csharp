using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;

namespace ClassWork 
{
    enum TypePizza { Cheese, Pepperoni, Clam, Veggie }
    enum StylePizza { NY, Chicago }

    abstract class PizzaStore 
    {
        public abstract Pizza CreatePizza(TypePizza typePizza);
    }
    class NYPizzaStore : PizzaStore
    {
        public override Pizza CreatePizza(TypePizza typePizza)
        {
            if (typePizza == TypePizza.Cheese)
            {
                return new NYStyleCheesePizza();
            }
            else if (typePizza == TypePizza.Pepperoni)
            {
                return new NYStylePepperoniPizza();
            }
            else if (typePizza == TypePizza.Clam)
            {
                return new NYStyleClamPizza();
            }
            else if (typePizza == TypePizza.Veggie)
            {
                return new NYStyleVeggiePizza();
            }
            else
                throw new Exception("Error type pizza");
        }
    }
    class ChicagoPizzaStore : PizzaStore
    {
        public override Pizza CreatePizza(TypePizza typePizza)
        {
            if (typePizza == TypePizza.Cheese)
            {
                return new ChicagoStyleCheesePizza();
            }
            else if (typePizza == TypePizza.Pepperoni)
            {
                return new ChicagoStylePepperoniPizza();
            }
            else if (typePizza == TypePizza.Clam)
            {
                return new ChicagoStyleClamPizza();
            }
            else if (typePizza == TypePizza.Veggie)
            {
                return new ChicagoStyleVeggiePizza();
            }
            else
                throw new Exception("Error");
        }
    }

    abstract class Pizza {}
    class ChicagoStyleCheesePizza : Pizza { }
    class ChicagoStylePepperoniPizza : Pizza { }
    class ChicagoStyleClamPizza : Pizza { }
    class ChicagoStyleVeggiePizza : Pizza { }
    class NYStyleCheesePizza : Pizza { }
    class NYStylePepperoniPizza : Pizza { }
    class NYStyleClamPizza : Pizza { }
    class NYStyleVeggiePizza : Pizza { }

    class ClientApplication
    {
        List<Pizza> OrderList { get; set; }
        PizzaStore PizzaStore { get; set; }
        public ClientApplication()
        {
            OrderList = new List<Pizza>();
        }
        public void MakeOrder(TypePizza typePizza, StylePizza stylePizza) 
        {
            if (stylePizza == StylePizza.NY)
                PizzaStore = new NYPizzaStore();
            else if (stylePizza == StylePizza.Chicago)
                PizzaStore = new ChicagoPizzaStore();
            else
                throw new Exception("Error style pizza");

            OrderList.Add(PizzaStore.CreatePizza(typePizza));
        }
        public void PrintInfo() 
        {
            Console.WriteLine($"Order:");
            foreach (var item in OrderList)
            {
                Console.WriteLine($"\t{item.GetType().Name}");
            }
            Console.WriteLine();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            ClientApplication clientApplication1 = new ClientApplication();
            clientApplication1.MakeOrder(TypePizza.Cheese, StylePizza.NY);
            clientApplication1.MakeOrder(TypePizza.Clam, StylePizza.Chicago);

            ClientApplication clientApplication2 = new ClientApplication();
            clientApplication2.MakeOrder(TypePizza.Pepperoni, StylePizza.NY);
            clientApplication2.MakeOrder(TypePizza.Veggie, StylePizza.Chicago);

            clientApplication1.PrintInfo();
            clientApplication2.PrintInfo();
        }
    }
}

