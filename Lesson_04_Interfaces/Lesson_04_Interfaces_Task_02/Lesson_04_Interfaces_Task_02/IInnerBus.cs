﻿
namespace Lesson_04_Interfaces_Task_02
{
    interface IInnerBus : IBus
    {
        const int MAXSPEEDTRANSFERDATA = 128000;
    }
}