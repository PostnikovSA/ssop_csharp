﻿
namespace Lesson_04_Interfaces_Task_02
{
    class Scanner : IUsbBus
    {
        public int MaxSpeedDataTransferToTheMotherBoard { get { return IUsbBus.MAXSPEEDTRANSFERDATA; } }
        public MotherboardConnectionStatus motherboardConnectStatus { get; set; }

        public Scanner()
        {
            motherboardConnectStatus = MotherboardConnectionStatus.Off;
        }
    }
}