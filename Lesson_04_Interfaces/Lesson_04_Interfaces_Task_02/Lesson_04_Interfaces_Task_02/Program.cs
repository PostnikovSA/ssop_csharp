﻿/*
Lesson_04_Interfaces_Task_02

Реализовать компоненты компьютерной системы, связанные между собой через определенные интерфейсы.
Обеспечить возможность стыковки элементов системы между собой в случае совпадения интерфейсов взаимодействия. 
Интерфейсы в обязательном порядке должны поддерживать информацию о максимальной скорости передачи данных и 
возможность передавать как минимум побайтовые данные.

Интерфейсы:
    IUsbBus (шина USB)
    ISata (шина SATA)
    INetwork (сеть)
    IInnerBus (внутренняя шина компьютера)
Классы:
    MotherBoard (материнская плата с процессором)
    RamMemory (оперативная память)
    HardDisk (жесткий диск)
    Printer (принтер)
    Scanner (сканер изображений)
    NetworkCard (сетевая карта)
    Keyboard (клавиатура) 
*/

using System;

namespace Lesson_04_Interfaces_Task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            MotherBoard motherBoard1 = new MotherBoard();

            RamMemory ramMemory1 = new RamMemory();
            HardDisk hardDisk1 = new HardDisk();
            Printer printer1 = new Printer();
            Scanner scanner1 = new Scanner();
            NetworkCard networkCard1 = new NetworkCard();
            Keyboard keyboard1 = new Keyboard();

            motherBoard1.PrintInfo();
            Console.WriteLine();

            motherBoard1.ConnectBus(ramMemory1);
            motherBoard1.ConnectBus(hardDisk1);
            motherBoard1.ConnectBus(printer1);
            motherBoard1.ConnectBus(scanner1);
            motherBoard1.ConnectBus(networkCard1);
            motherBoard1.ConnectBus(keyboard1);

            motherBoard1.PrintInfo();
            Console.WriteLine();

            motherBoard1.TransferData(ramMemory1, 6000);
            motherBoard1.TransferData(hardDisk1, 300);
            motherBoard1.TransferData(printer1, 5600);
            motherBoard1.TransferData(scanner1, 4320);
            motherBoard1.TransferData(networkCard1, 200);
            motherBoard1.TransferData(keyboard1, 5000);

            motherBoard1.PrintInfo();
        }
    }
}