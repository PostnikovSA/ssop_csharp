﻿
namespace Lesson_04_Interfaces_Task_02
{
    class NetworkCard : IInnerBus, INetwork
    {
        public int MaxSpeedDataTransferToTheMotherBoard { get { return IInnerBus.MAXSPEEDTRANSFERDATA; } }
        public int MaxSpeedDataTransferToTheNetwork { get { return INetwork.MAXSPEEDTRANSFERDATA; } }
        public MotherboardConnectionStatus motherboardConnectStatus { get; set; }
        public NetworkConnectionStatus networkConnectStatus { get; set; }
        public NetworkCard()
        {
            motherboardConnectStatus = MotherboardConnectionStatus.Off;
            networkConnectStatus = NetworkConnectionStatus.Off;
        }
    }
}