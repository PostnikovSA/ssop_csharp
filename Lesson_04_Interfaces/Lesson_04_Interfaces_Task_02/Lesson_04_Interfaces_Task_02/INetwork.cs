﻿
namespace Lesson_04_Interfaces_Task_02
{
    interface INetwork
    {
        const int MAXSPEEDTRANSFERDATA = 100000;
        NetworkConnectionStatus networkConnectStatus { get; set; }
        int MaxSpeedDataTransferToTheNetwork { get; }
    }
}