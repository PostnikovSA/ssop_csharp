﻿
namespace Lesson_04_Interfaces_Task_02
{
    class Printer : IUsbBus
    {
        public int MaxSpeedDataTransferToTheMotherBoard { get { return IUsbBus.MAXSPEEDTRANSFERDATA; } }
        public MotherboardConnectionStatus motherboardConnectStatus { get; set; }

        public Printer()
        {
            motherboardConnectStatus = MotherboardConnectionStatus.Off;
        }
    }
}