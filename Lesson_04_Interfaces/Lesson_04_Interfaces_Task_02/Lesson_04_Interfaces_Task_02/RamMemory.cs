﻿
namespace Lesson_04_Interfaces_Task_02
{
    class RamMemory : IInnerBus
    {
        public int MaxSpeedDataTransferToTheMotherBoard { get { return IInnerBus.MAXSPEEDTRANSFERDATA; } }
        public MotherboardConnectionStatus motherboardConnectStatus { get; set; }

        public RamMemory()
        {
            motherboardConnectStatus = MotherboardConnectionStatus.Off;
        }
    }
}