﻿
namespace Lesson_04_Interfaces_Task_02
{
    interface IBus
    {
        MotherboardConnectionStatus motherboardConnectStatus { get; set; }
        int MaxSpeedDataTransferToTheMotherBoard { get; }
    }
}