﻿
namespace Lesson_04_Interfaces_Task_02
{
    class Keyboard : IUsbBus
    {
        public int MaxSpeedDataTransferToTheMotherBoard { get { return IUsbBus.MAXSPEEDTRANSFERDATA; } }
        public MotherboardConnectionStatus motherboardConnectStatus { get; set; }

        public Keyboard()
        {
            motherboardConnectStatus = MotherboardConnectionStatus.Off;
        }
    }
}