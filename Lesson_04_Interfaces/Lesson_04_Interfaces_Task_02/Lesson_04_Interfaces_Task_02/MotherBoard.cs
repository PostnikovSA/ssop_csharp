﻿
using System;
using System.Collections;

namespace Lesson_04_Interfaces_Task_02
{
    class MotherBoard : IPrintInfo
    {
        ArrayList _connectList;
        ArrayList _processList;
        public MotherBoard()
        {
            _connectList = new ArrayList();
            _processList = new ArrayList();
        }
        public void PrintInfo() 
        {
            Console.WriteLine($"list of devices connected to the {this.GetType().Name}:");
            if (_connectList.Count == 0)
                Console.WriteLine($"\tNo connected devices");
            else 
            {
                foreach (var item in _connectList)
                {
                    Console.WriteLine($"\t{item.GetType().Name}");
                }
            }
            Console.WriteLine();

            Console.WriteLine($"list of active processes running on the {this.GetType().Name}:");
            if (_processList.Count == 0)
                Console.WriteLine($"\tNo active processes");
            for (int i = 0; i < _processList.Count; i++)
            {
                Console.WriteLine($"\tProcess: {i+1} {_connectList[i].GetType().Name} {_processList[i]} Gbit/s");
            }
            Console.WriteLine();
        }
        public void ConnectBus<T>(T iConnectBus) where T : IBus
        {
            if (iConnectBus.motherboardConnectStatus == MotherboardConnectionStatus.Off)
            {
                iConnectBus.motherboardConnectStatus = MotherboardConnectionStatus.On;
                _connectList.Add(iConnectBus);
            }
            else
                Console.WriteLine($"Error connection. {iConnectBus.GetType().Name} already connected.");
        }
        public void TransferData<T>(T iConnectBus, int data) where T : IBus
        {
            if (iConnectBus.motherboardConnectStatus == MotherboardConnectionStatus.Off) 
                Console.WriteLine($"Error. This {iConnectBus.GetType().Name} no connected to the {this.GetType().Name}");
            else if (data > iConnectBus.MaxSpeedDataTransferToTheMotherBoard)
                Console.WriteLine($"Error. Max speed transfer data on this bus: {iConnectBus.MaxSpeedDataTransferToTheMotherBoard} Gbit/s\n");
            else
                this._processList.Add(data);
        }
    }
}