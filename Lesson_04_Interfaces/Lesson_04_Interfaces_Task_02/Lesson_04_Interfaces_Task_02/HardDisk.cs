﻿
namespace Lesson_04_Interfaces_Task_02
{
    class HardDisk : ISataBus
    {
        public int MaxSpeedDataTransferToTheMotherBoard { get { return ISataBus.MAXSPEEDTRANSFERDATA; } }
        public MotherboardConnectionStatus motherboardConnectStatus { get; set; }

        public HardDisk()
        {
            motherboardConnectStatus = MotherboardConnectionStatus.Off;
        }
    }
}