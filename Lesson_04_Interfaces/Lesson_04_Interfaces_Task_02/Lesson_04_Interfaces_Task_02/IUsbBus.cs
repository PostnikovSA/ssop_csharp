﻿
namespace Lesson_04_Interfaces_Task_02
{
    interface IUsbBus : IBus
    {
        const int MAXSPEEDTRANSFERDATA = 20000;
    }
}