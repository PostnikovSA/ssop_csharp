﻿/*
Lesson_04_Interfaces_Task_01

Реализовать системы электрических источников и приборов, соединенных между собой через шнуры. 
В интерфейсах должны быть предусмотрена возможность получения информации о напряжении 
и максимальной мощности, которую поддерживает элемент. 
Прибор должен иметь наименование, потребляемую мощность, а источник и провод – списки подключенных приборов.

Интерфейсы: 
	IElectricSource (источник тока)
	IElectricAppliance (электрический прибор)
	IElectricWire (электрический шнур)
Классы:
	SolarBattery (солнечная батарея)
	DieselGenerator (дизельный генератор)
	NuclearPowerPlant (атомная электростанция)
	Kettle (чайник)
	Iron (утюг)
	Lathe (токарный станок)
	Refrigerator (холодильник)
	ElectricPowerStrip (электрический удлинитель)
	HighLine (высоковольтная линия)
	StepDownTransformer (понижающий трансформатор, должен реализовывать интерфейсы и потребителя и источника тока)
*/

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Lesson_04_Interfaces_Task_01
{
	enum WorkStatus
	{
		On,
		Off
	}
	interface IElectricInfo 
	{
		void PrintInfo();
	}
	interface IElectricAppliance
	{
        string ModelName { get; }
        int VoltageType { get; }
        long PowerConsumption { get; }
		WorkStatus WorkStatus { get; set; }
		IElectricSource ObjConnectedSource { get; set; }
		IElectricWire ObjConnectedWire { get; set; }
		void ConnectToPowerSupply(IElectricSource electricSource, IElectricWire electricWire);
		void DisconnectFromPowerSupply();
	}
	interface IElectricSource
	{
        string ModelName { get; }
        long MaxPowerReleased { get; }

        long PowerReleased { get; set; }
		int VoltageType { get; }
		WorkStatus WorkStatus { get; set; }
		List<IElectricAppliance> ListConnectedElectricAppliance { get; set; }
	}
	interface IElectricWire 
	{
        string ModelName { get; }
		long MaxPowerTransfer { get; }
        long PowerTransfer { get; set; }
		int VoltageType { get; }
		List<IElectricAppliance> ListConnectedElectricAppliance { get; set; }
	}
	class NuclearPowerPlant : IElectricSource, IElectricInfo
	{
        public string ModelName { get; }
		public long MaxPowerReleased { get; set; }
        public long PowerReleased { get; set; }
		public int VoltageType { get; }
		public WorkStatus WorkStatus { get; set; }
		public List<IElectricAppliance> ListConnectedElectricAppliance { get; set; }
		public NuclearPowerPlant(string modelName)
		{
			ModelName = modelName;
			MaxPowerReleased = 6000000000;
			PowerReleased = MaxPowerReleased;
			VoltageType = 50000;
			WorkStatus = WorkStatus.Off;
			ListConnectedElectricAppliance = new List<IElectricAppliance>();
		}
		public void PrintInfo()
		{
			Console.WriteLine
				(
				$"Class name:\t\t{this.GetType().Name}\n" +
				$"Model name: \t\t{ModelName}\n" +
				$"Voltage type:\t\t{VoltageType}v\n" +
				$"Power released:\t\t{PowerReleased}w / {MaxPowerReleased}w\n" +
				$"Work status:\t\t{WorkStatus}\n" +
				$"List connected electric appliance:"
				);

			if (ListConnectedElectricAppliance.Count == 0)
				Console.WriteLine("\tNo connected electrical appliances\n");
			else 
			{
				foreach (var item in ListConnectedElectricAppliance)
				{
					Console.WriteLine($"\t-{item.PowerConsumption}w / {item.GetType().Name} / {item.ModelName}");
				}
				Console.WriteLine();
			}
		}
	}
	class DieselGenerator : IElectricSource, IElectricInfo
	{
        public string ModelName { get; }
		public long MaxPowerReleased { get; }
        public long PowerReleased { get; set; }
		public int VoltageType { get; }
		public WorkStatus WorkStatus { get; set; }
		public List<IElectricAppliance> ListConnectedElectricAppliance { get; set; }
		public DieselGenerator(string modelName)
		{
			ModelName = modelName;
			MaxPowerReleased = 5000;
			PowerReleased = MaxPowerReleased;
			VoltageType = 220;
			WorkStatus = WorkStatus.Off;
			ListConnectedElectricAppliance = new List<IElectricAppliance>();
		}
		public void PrintInfo()
		{
			Console.WriteLine
(
				$"Class name:\t\t{this.GetType().Name}\n" +
				$"Model name:\t\t{ ModelName}\n" +
				$"Voltage type:\t\t{VoltageType}v\n" +
				$"Power released:\t\t{PowerReleased}w / {MaxPowerReleased}w\n" +
				$"Work status:\t\t{WorkStatus}\n" +
				$"List connected electric appliance:"
				);

			if (ListConnectedElectricAppliance.Count == 0)
				Console.WriteLine("\tNo connected electrical appliances\n");
			else
			{
				foreach (var item in ListConnectedElectricAppliance)
				{
					Console.WriteLine($"\t-{item.PowerConsumption}w / {item.GetType().Name} / {item.ModelName}");
				}
				Console.WriteLine();
			}
		}
	}
	class SolarBattery : IElectricSource, IElectricInfo
	{
		public string ModelName { get; }
		public long MaxPowerReleased { get; }
		public long PowerReleased { get; set; }
		public int VoltageType { get; }
		public WorkStatus WorkStatus { get; set; }
		public List<IElectricAppliance> ListConnectedElectricAppliance { get; set; }
		public SolarBattery(string modelName)
		{
			ModelName = modelName;
			MaxPowerReleased = 500;
			PowerReleased = MaxPowerReleased;
			VoltageType = 220;
			WorkStatus = WorkStatus.Off;
			ListConnectedElectricAppliance = new List<IElectricAppliance>();
		}
		public void PrintInfo()
		{
			Console.WriteLine
(
				$"Class name:\t\t{this.GetType().Name}\n" +
				$"Model name:\t\t{ ModelName}\n" +
				$"Voltage type:\t\t{VoltageType}v\n" +
				$"Power released:\t\t{PowerReleased}w / {MaxPowerReleased}w\n" +
				$"Work status:\t\t{WorkStatus}\n" +
				$"List connected electric appliance:"
				);

			if (ListConnectedElectricAppliance.Count == 0)
				Console.WriteLine("\tNo connected electrical appliances\n");
			else
			{
				foreach (var item in ListConnectedElectricAppliance)
				{
					Console.WriteLine($"\t-{item.PowerConsumption}w / {item.GetType().Name} / {item.ModelName}");
				}
				Console.WriteLine();
			}
		}
	}
	class HighLine : IElectricWire, IElectricInfo
	{
        public string ModelName { get; }
        public int VoltageType { get; }
		public long MaxPowerTransfer { get; }
		public long PowerTransfer { get; set; }
		public List<IElectricAppliance> ListConnectedElectricAppliance { get; set; }
		public HighLine(string modelName)
		{
			ModelName = modelName;
			MaxPowerTransfer = 700000;
			PowerTransfer = MaxPowerTransfer;
			VoltageType = 50000;
			ListConnectedElectricAppliance = new List<IElectricAppliance>();
		}
		public void PrintInfo()
		{
			Console.WriteLine
				(
				$"Class name:\t\t{this.GetType().Name}\n" +
				$"Model name:\t\t{ModelName}\n" +
				$"Voltage type:\t\t{VoltageType}v\n" +
				$"Power transfer:\t\t{PowerTransfer}w / {MaxPowerTransfer}w\n" +
				$"List connected electric appliance:"
				);

			if (ListConnectedElectricAppliance.Count == 0)
				Console.WriteLine("\tNo connected electrical appliances\n");
			else
			{
				foreach (var item in ListConnectedElectricAppliance)
				{
					Console.WriteLine($"\t-{item.PowerConsumption}w / {item.GetType().Name} / {item.ModelName}");
				}
				Console.WriteLine();
			}
		}
	}
	class ElectricPowerStrip : IElectricWire, IElectricInfo
	{
        public string ModelName { get; }
        public int VoltageType { get; }
        public long MaxPowerTransfer { get; }
        public long PowerTransfer { get; set; }
		public List<IElectricAppliance> ListConnectedElectricAppliance { get; set; }
		public ElectricPowerStrip(string modelName, int voltageType)
		{
			if ((voltageType == 220) || (voltageType == 380))
				VoltageType = voltageType;
			else
				throw new ArgumentException("Error. Enter voltage 220 or 380");

			ModelName = modelName;
			MaxPowerTransfer = 12000;
			PowerTransfer = MaxPowerTransfer;
			ListConnectedElectricAppliance = new List<IElectricAppliance>();
		}
		public void PrintInfo()
		{
			Console.WriteLine
				(
				$"Class name:\t\t{this.GetType().Name}\n" +
				$"Model name:\t\t{ModelName}\n" +
				$"Voltage type:\t\t{VoltageType}v\n" +
				$"Power transfer:\t\t{PowerTransfer}w / {MaxPowerTransfer}w\n" +
				$"List connected electric appliance:"
				);

			if (ListConnectedElectricAppliance.Count == 0)
				Console.WriteLine("\tNo connected electrical appliances\n");
			else
			{
				foreach (var item in ListConnectedElectricAppliance)
				{
					Console.WriteLine($"\t-{item.PowerConsumption}w / {item.GetType().Name} / {item.ModelName}");
				}
				Console.WriteLine();
			}
		}
	}
    class StepDownTransformer : IElectricAppliance, IElectricSource, IElectricInfo
    {
        public string ModelName { get; }
        public long MaxPowerReleased { get; }
        public long PowerReleased { get; set; }
        public long PowerConsumption { get; }
        public int VoltageType { get; }
        public WorkStatus WorkStatus { get; set; }
		public IElectricSource ObjConnectedSource { get; set; }
		public IElectricWire ObjConnectedWire { get; set; }
		public List<IElectricAppliance> ListConnectedElectricAppliance { get; set; }
        public StepDownTransformer(string modelName)
        {
			ModelName = modelName;
			VoltageType = 50000;
			PowerConsumption = 630000;
			MaxPowerReleased = PowerConsumption;
			PowerReleased = MaxPowerReleased;
			WorkStatus = WorkStatus.Off;
			ListConnectedElectricAppliance = new List<IElectricAppliance>();
        }
		public void PrintInfo()
		{
			Console.WriteLine
(
				$"Class name:\t\t{this.GetType().Name}\n" +
				$"Model name:\t\t{ ModelName}\n" +
				$"Voltage type:\t\t{VoltageType}v\n" +
				$"Power released:\t\t{PowerReleased}w / {MaxPowerReleased}w\n" +
				$"Work status:\t\t{WorkStatus}\n" +
				$"List connected electric appliance:"
				);

			if (ListConnectedElectricAppliance.Count == 0)
				Console.WriteLine("\tNo connected electrical appliances\n");
			else
			{
				foreach (var item in ListConnectedElectricAppliance)
				{
					Console.WriteLine($"\t-{item.PowerConsumption}w / {item.GetType().Name} / {item.ModelName}");
				}
				Console.WriteLine();
			}
		}
		public void ConnectToPowerSupply(IElectricSource electricSource, IElectricWire electricWire)
		{
			bool isError = false;

			if (electricSource.PowerReleased < this.PowerConsumption)
			{
				isError = true;
				Console.WriteLine($"Error. Power released {electricSource.ModelName} to low\n");
			}
			if (electricWire.PowerTransfer < this.PowerConsumption)
			{
				isError = true;
				Console.WriteLine($"Error. Power transfered {electricWire.ModelName} to low\n");
			}
			//if (electricWire.VoltageType != this.VoltageType)
			//{
			//	isError = true;
			//	Console.WriteLine($"Error. Voltage type {electricWire.ModelName} different\n");
			//}
			//if ((electricSource.VoltageType != this.VoltageType) && (electricSource.VoltageType != 50000))
			//{
			//	isError = true;
			//	Console.WriteLine($"Error. Voltage type {electricSource.ModelName} different\n");
			//}
			if (this.WorkStatus == WorkStatus.On)
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already to connected\n");
			}
			if (electricSource.WorkStatus == WorkStatus.Off)
			{
				isError = true;
				Console.WriteLine("Error. Electric source as work status Off\n");
			}
			if (ObjConnectedSource != null)
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already connected\n");
			}
			if ((electricWire.ListConnectedElectricAppliance.Count != 0) && (electricSource != ObjConnectedSource))
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already connected!\n");
			}

			if (isError == false)
			{
				this.WorkStatus = WorkStatus.On;

				electricSource.PowerReleased -= this.PowerConsumption;
				electricSource.ListConnectedElectricAppliance.Add(this);

				electricWire.PowerTransfer -= this.PowerConsumption;
				electricWire.ListConnectedElectricAppliance.Add(this);

				ObjConnectedSource = electricSource;
				ObjConnectedWire = electricWire;
			}
		}
		public void DisconnectFromPowerSupply()
		{
			bool isError = false;

			if (this.WorkStatus == WorkStatus.Off)
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already no connected\n");
			}

			if (isError == false)
			{
				this.WorkStatus = WorkStatus.Off;

				ObjConnectedSource.PowerReleased += this.PowerConsumption;
				ObjConnectedSource.ListConnectedElectricAppliance.Remove(this);
				ObjConnectedSource = null;

				ObjConnectedWire.PowerTransfer += this.PowerConsumption;
				ObjConnectedWire.ListConnectedElectricAppliance.Remove(this);
				ObjConnectedWire = null;
			}
		}
	}
    class Kettle : IElectricAppliance, IElectricInfo
	{
		public string ModelName { get; }
		public int VoltageType { get; }
		public long PowerConsumption { get; }
		public WorkStatus WorkStatus { get; set; }
		public IElectricSource ObjConnectedSource { get; set; }
		public IElectricWire ObjConnectedWire { get; set; }
		public Kettle(string modelName)
        {
			ModelName = modelName;
			VoltageType = 220;
			PowerConsumption = 2200;
			WorkStatus = WorkStatus.Off;
        }
		public void PrintInfo()
		{
            Console.WriteLine(
				$"Class name:\t\t{this.GetType().Name}\n" +
				$"Model name:\t\t{ModelName}\n" +
				$"Voltage type:\t\t{VoltageType}v\n" +
				$"Power consumption:\t{PowerConsumption}w\n" +
				$"Work status:\t\t{WorkStatus}\n");
		}
		public void ConnectToPowerSupply(IElectricSource electricSource, IElectricWire electricWire) 
		{
			bool isError = false;

			if (electricSource.PowerReleased < this.PowerConsumption) 
			{
				isError = true;
				Console.WriteLine($"Error. Power released {electricSource.ModelName} to low\n");
			}
			if (electricWire.PowerTransfer < this.PowerConsumption)
			{
				isError = true;
				Console.WriteLine($"Error. Power transfered {electricWire.ModelName} to low\n");
			}
			if (electricWire.VoltageType != this.VoltageType)
			{
				isError = true;
				Console.WriteLine($"Error. Voltage type {electricWire.ModelName} different\n");
			}
			if ((electricSource.VoltageType != this.VoltageType) && (electricSource.VoltageType != 50000))
			{
				isError = true;
				Console.WriteLine($"Error. Voltage type {electricSource.ModelName} different\n");
			}
			if (this.WorkStatus == WorkStatus.On)
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already to connected\n");
            }
            if (electricSource.WorkStatus == WorkStatus.Off)
			{
				isError = true;
				Console.WriteLine("Error. Electric source as work status Off\n");
			}
			if (ObjConnectedSource != null) 
			{
				isError = true;
                Console.WriteLine($"Error. This {this.GetType().Name} already connected\n");
			}
			if ((electricWire.ListConnectedElectricAppliance.Count != 0) && (electricSource != ObjConnectedSource))
            {
                isError = true;
                Console.WriteLine($"Error. This {this.GetType().Name} already connected!\n");
            }

            if (isError == false) 
			{
				this.WorkStatus = WorkStatus.On;

				electricSource.PowerReleased -= this.PowerConsumption;
				electricSource.ListConnectedElectricAppliance.Add(this);

				electricWire.PowerTransfer -= this.PowerConsumption;
				electricWire.ListConnectedElectricAppliance.Add(this);

				ObjConnectedSource = electricSource;
				ObjConnectedWire = electricWire;
			}
		}
		public void DisconnectFromPowerSupply()
		{
			bool isError = false;
			
			if (this.WorkStatus == WorkStatus.Off)
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already no connected\n");
			}
			
			if (isError == false)
			{
				this.WorkStatus = WorkStatus.Off;

				ObjConnectedSource.PowerReleased += this.PowerConsumption;
				ObjConnectedSource.ListConnectedElectricAppliance.Remove(this);
				ObjConnectedSource = null;

				ObjConnectedWire.PowerTransfer += this.PowerConsumption;
				ObjConnectedWire.ListConnectedElectricAppliance.Remove(this);
				ObjConnectedWire = null;
			}
		}
	}
	class Iron : IElectricAppliance, IElectricInfo
	{
		public string ModelName { get; }
		public int VoltageType { get; }
		public long PowerConsumption { get; }
		public WorkStatus WorkStatus { get; set; }
		public IElectricSource ObjConnectedSource { get; set; }
		public IElectricWire ObjConnectedWire { get; set; }
		public Iron(string modelName)
		{
			ModelName = modelName;
			VoltageType = 220;
			PowerConsumption = 2000;
			WorkStatus = WorkStatus.Off;
		}
		public void PrintInfo()
		{
			Console.WriteLine(
				$"Class name:\t\t{this.GetType().Name}\n" +
				$"Model name:\t\t{ModelName}\n" +
				$"Voltage type:\t\t{VoltageType}v\n" +
				$"Power consumption:\t{PowerConsumption}w\n" +
				$"Work status:\t\t{WorkStatus}\n");
		}
		public void ConnectToPowerSupply(IElectricSource electricSource, IElectricWire electricWire)
		{
			bool isError = false;

			if (electricSource.PowerReleased < this.PowerConsumption)
			{
				isError = true;
				Console.WriteLine($"Error. Power released {electricSource.ModelName} to low\n");
			}
			if (electricWire.PowerTransfer < this.PowerConsumption)
			{
				isError = true;
				Console.WriteLine($"Error. Power transfered {electricWire.ModelName} to low\n");
			}
			if (electricWire.VoltageType != this.VoltageType)
			{
				isError = true;
				Console.WriteLine($"Error. Voltage type {electricWire.ModelName} different\n");
			}
			if ((electricSource.VoltageType != this.VoltageType) && (electricSource.VoltageType != 50000))
			{
				isError = true;
				Console.WriteLine($"Error. Voltage type {electricSource.ModelName} different\n");
			}
			if (this.WorkStatus == WorkStatus.On)
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already to connected\n");
			}
			if (electricSource.WorkStatus == WorkStatus.Off)
			{
				isError = true;
				Console.WriteLine("Error. Electric source as work status Off\n");
			}
			if (ObjConnectedSource != null)
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already connected\n");
			}
			if ((electricWire.ListConnectedElectricAppliance.Count != 0) && (electricSource != ObjConnectedSource))
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already connected!\n");
			}

			if (isError == false)
			{
				this.WorkStatus = WorkStatus.On;

				electricSource.PowerReleased -= this.PowerConsumption;
				electricSource.ListConnectedElectricAppliance.Add(this);

				electricWire.PowerTransfer -= this.PowerConsumption;
				electricWire.ListConnectedElectricAppliance.Add(this);

				ObjConnectedSource = electricSource;
				ObjConnectedWire = electricWire;
			}
		}
		public void DisconnectFromPowerSupply()
		{
			bool isError = false;

			if (this.WorkStatus == WorkStatus.Off)
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already no connected\n");
			}

			if (isError == false)
			{
				this.WorkStatus = WorkStatus.Off;

				ObjConnectedSource.PowerReleased += this.PowerConsumption;
				ObjConnectedSource.ListConnectedElectricAppliance.Remove(this);
				ObjConnectedSource = null;

				ObjConnectedWire.PowerTransfer += this.PowerConsumption;
				ObjConnectedWire.ListConnectedElectricAppliance.Remove(this);
				ObjConnectedWire = null;
			}
		}
	}
	class Refrigerator : IElectricAppliance, IElectricInfo
	{
		public string ModelName { get; }
		public int VoltageType { get; }
		public long PowerConsumption { get; }
		public WorkStatus WorkStatus { get; set; }
		public IElectricSource ObjConnectedSource { get; set; }
		public IElectricWire ObjConnectedWire { get; set; }
		public Refrigerator(string modelName)
		{
			ModelName = modelName;
			VoltageType = 220;
			PowerConsumption = 1100;
			WorkStatus = WorkStatus.Off;
		}
		public void PrintInfo()
		{
			Console.WriteLine(
				$"Class name:\t\t{this.GetType().Name}\n" +
				$"Model name:\t\t{ModelName}\n" +
				$"Voltage type:\t\t{VoltageType}v\n" +
				$"Power consumption:\t{PowerConsumption}w\n" +
				$"Work status:\t\t{WorkStatus}\n");
		}
		public void ConnectToPowerSupply(IElectricSource electricSource, IElectricWire electricWire)
		{
			bool isError = false;

			if (electricSource.PowerReleased < this.PowerConsumption)
			{
				isError = true;
				Console.WriteLine($"Error. Power released {electricSource.ModelName} to low\n");
			}
			if (electricWire.PowerTransfer < this.PowerConsumption)
			{
				isError = true;
				Console.WriteLine($"Error. Power transfered {electricWire.ModelName} to low\n");
			}
			if (electricWire.VoltageType != this.VoltageType)
			{
				isError = true;
				Console.WriteLine($"Error. Voltage type {electricWire.ModelName} different\n");
			}
			if ((electricSource.VoltageType != this.VoltageType) && (electricSource.VoltageType != 50000))
			{
				isError = true;
				Console.WriteLine($"Error. Voltage type {electricSource.ModelName} different\n");
			}
			if (this.WorkStatus == WorkStatus.On)
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already to connected\n");
			}
			if (electricSource.WorkStatus == WorkStatus.Off)
			{
				isError = true;
				Console.WriteLine("Error. Electric source as work status Off\n");
			}
			if (ObjConnectedSource != null)
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already connected\n");
			}
			if ((electricWire.ListConnectedElectricAppliance.Count != 0) && (electricSource != ObjConnectedSource))
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already connected!\n");
			}

			if (isError == false)
			{
				this.WorkStatus = WorkStatus.On;

				electricSource.PowerReleased -= this.PowerConsumption;
				electricSource.ListConnectedElectricAppliance.Add(this);

				electricWire.PowerTransfer -= this.PowerConsumption;
				electricWire.ListConnectedElectricAppliance.Add(this);

				ObjConnectedSource = electricSource;
				ObjConnectedWire = electricWire;
			}
		}
		public void DisconnectFromPowerSupply()
		{
			bool isError = false;

			if (this.WorkStatus == WorkStatus.Off)
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already no connected\n");
			}

			if (isError == false)
			{
				this.WorkStatus = WorkStatus.Off;

				ObjConnectedSource.PowerReleased += this.PowerConsumption;
				ObjConnectedSource.ListConnectedElectricAppliance.Remove(this);
				ObjConnectedSource = null;

				ObjConnectedWire.PowerTransfer += this.PowerConsumption;
				ObjConnectedWire.ListConnectedElectricAppliance.Remove(this);
				ObjConnectedWire = null;
			}
		}
	}
	class Lathe : IElectricAppliance, IElectricInfo
	{
		public string ModelName { get; }
		public int VoltageType { get; }
		public long PowerConsumption { get; }
		public WorkStatus WorkStatus { get; set; }
		public IElectricSource ObjConnectedSource { get; set; }
		public IElectricWire ObjConnectedWire { get; set; }
		public Lathe(string modelName)
		{
			ModelName = modelName;
			VoltageType = 380;
			PowerConsumption = 10000;
			WorkStatus = WorkStatus.Off;
		}
		public void PrintInfo()
		{
			Console.WriteLine(
				$"Class name:\t\t{this.GetType().Name}\n" +
				$"Model name:\t\t{ModelName}\n" +
				$"Voltage type:\t\t{VoltageType}v\n" +
				$"Power consumption:\t{PowerConsumption}w\n" +
				$"Work status:\t\t{WorkStatus}\n");
		}
		public void ConnectToPowerSupply(IElectricSource electricSource, IElectricWire electricWire)
		{
			bool isError = false;

			if (electricSource.PowerReleased < this.PowerConsumption)
			{
				isError = true;
				Console.WriteLine($"Error. Power released {electricSource.ModelName} to low\n");
			}
			if (electricWire.PowerTransfer < this.PowerConsumption)
			{
				isError = true;
				Console.WriteLine($"Error. Power transfered {electricWire.ModelName} to low\n");
			}
			if (electricWire.VoltageType != this.VoltageType)
			{
				isError = true;
				Console.WriteLine($"Error. Voltage type {electricWire.ModelName} different\n");
			}
			if ((electricSource.VoltageType != this.VoltageType) && (electricSource.VoltageType != 50000))
			{
				isError = true;
				Console.WriteLine($"Error. Voltage type {electricSource.ModelName} different\n");
			}
			if (this.WorkStatus == WorkStatus.On)
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already to connected\n");
			}
			if (electricSource.WorkStatus == WorkStatus.Off)
			{
				isError = true;
				Console.WriteLine("Error. Electric source as work status Off\n");
			}
			if (ObjConnectedSource != null)
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already connected\n");
			}
			if ((electricWire.ListConnectedElectricAppliance.Count != 0) && (electricSource != ObjConnectedSource))
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already connected!\n");
			}

			if (isError == false)
			{
				this.WorkStatus = WorkStatus.On;

				electricSource.PowerReleased -= this.PowerConsumption;
				electricSource.ListConnectedElectricAppliance.Add(this);

				electricWire.PowerTransfer -= this.PowerConsumption;
				electricWire.ListConnectedElectricAppliance.Add(this);

				ObjConnectedSource = electricSource;
				ObjConnectedWire = electricWire;
			}
		}
		public void DisconnectFromPowerSupply()
		{
			bool isError = false;

			if (this.WorkStatus == WorkStatus.Off)
			{
				isError = true;
				Console.WriteLine($"Error. This {this.GetType().Name} already no connected\n");
			}

			if (isError == false)
			{
				this.WorkStatus = WorkStatus.Off;

				ObjConnectedSource.PowerReleased += this.PowerConsumption;
				ObjConnectedSource.ListConnectedElectricAppliance.Remove(this);
				ObjConnectedSource = null;

				ObjConnectedWire.PowerTransfer += this.PowerConsumption;
				ObjConnectedWire.ListConnectedElectricAppliance.Remove(this);
				ObjConnectedWire = null;
			}
		}
	}


	class Program
    {
        static void Main(string[] args)
        {
			NuclearPowerPlant nuclearPowerPlant1 = new NuclearPowerPlant("Fukusima");
			DieselGenerator dieselGenerator1 = new DieselGenerator("VimDizel");
			HighLine highLine1 = new HighLine("HighLiner");
			StepDownTransformer stepDownTransformer1 = new StepDownTransformer("Optimus");
			ElectricPowerStrip electricPowerStrip1 = new ElectricPowerStrip("Strptz", 220);
			Kettle kettle1 = new Kettle("TwoTeeFree");
			Lathe lathe1 = new Lathe("Latheti");
			ElectricPowerStrip electricPowerStrip2 = new ElectricPowerStrip("HighStrptz", 380);

			nuclearPowerPlant1.WorkStatus = WorkStatus.On;
			dieselGenerator1.WorkStatus = WorkStatus.On;

			stepDownTransformer1.ConnectToPowerSupply(nuclearPowerPlant1, highLine1);
			kettle1.ConnectToPowerSupply(stepDownTransformer1, electricPowerStrip1);
			lathe1.ConnectToPowerSupply(stepDownTransformer1, electricPowerStrip2);

			nuclearPowerPlant1.PrintInfo();
			stepDownTransformer1.PrintInfo();
		}
    }
}
