﻿/*
Lesson_04_Interfaces_Task_03

Реализовать набор коллекций, реализующих стандартные интерфейсы 
по работе с коллекциями из пространства имен System.Collections.

Интерфейсы:
	IEnumerable (последовательность элементов)
	ICollection (коллекция)
	IList (список)
	IDictionary (словарь)
Классы:
	List (список)
	Queue (очередь)
	Dictionary (словарь)
*/

using System;
//using System.Collections;
//using System.Collections.Generic;

namespace Lesson_04_Interfaces_Task_03
{
    interface IEnumerable { }
    interface ICollection : IEnumerable { }
    interface IList : ICollection { }
    interface IQueue : ICollection { }
    interface IDictionary : ICollection { }
    class List : IList
    {
        public List()
        {
            Console.WriteLine($"{this.GetType().Name} : \t\tIEnumerable, ICollection, IList\n");
        }
    }
    class Queue : IQueue 
    {
        public Queue()
        {
            Console.WriteLine($"{this.GetType().Name} : \tIEnumerable, ICollection, IQueue\n");
        }
    }
    class Dictionary : IDictionary 
    {
        public Dictionary()
        {
            Console.WriteLine($"{this.GetType().Name} : \tIEnumerable, ICollection, IDictionary\n");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List list = new List();
            Queue queue = new Queue();
            Dictionary dictionary = new Dictionary();
        }
    }
}
