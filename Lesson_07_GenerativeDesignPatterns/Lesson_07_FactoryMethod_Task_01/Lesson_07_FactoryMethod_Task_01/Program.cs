﻿/*
Lesson_07_FactoryMethod_Task_01

В презентации 4 - Введение в проектирование (часть 4) на слайде 11
приведена диаграмма классов ОО моделей, построенных в соответствии с паттернами Фабричный метод. 
Необходимо реализовать приведённые ОО модели, продемонстрировать работу реализованных классов.
*/

using System;

namespace Lesson_07_FactoryMethod_Task_01
{
    enum Config { Windows, Web }

    public abstract class Dialog
    {
        public abstract IButton CreateButton();
    }
    public class WindowsDialog : Dialog
    {
        public override IButton CreateButton() 
        {
            return new WindowsButton();
        }
    }
    public class WebDialog : Dialog 
    {
        public override IButton CreateButton()
        {
            return new HTMLButton();
        }
    }

    public interface IButton
    {
        void ButtonRender();
        void ButtonOnClick();
    }
    class WindowsButton : IButton
    {
        public void ButtonRender()
        {
            Console.WriteLine("Windows render");
        }
        public void ButtonOnClick()
        {
            Console.WriteLine("Windows Onclik");
        }
    }
    class HTMLButton : IButton
    {
        public void ButtonRender()
        {
            Console.WriteLine("HTML render");
        }
        public void ButtonOnClick()
        {
            Console.WriteLine("HTML Onclik");
        }
    }
    class ClientApplication 
    {
        Dialog Dialog { get; set; }
        public void Initialize(Config config) 
        {
            if (config == Config.Windows)
                Dialog = new WindowsDialog();
            else if (config == Config.Web)
                Dialog = new WebDialog();
            else
                throw new Exception("Error type config");
        }
        public void Render() 
        {
            IButton iButton = Dialog.CreateButton();
            iButton.ButtonRender();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            ClientApplication clientApplication = new ClientApplication();
            clientApplication.Initialize(Config.Windows);
            clientApplication.Render();
        }
    }
}
