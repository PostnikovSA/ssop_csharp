﻿/*
Lesson_07_AbstractFactory_Task_02

В презентации 4 - Введение в проектирование (часть 4) на слайде 24
приведена диаграмма классов ОО моделей, построенных в соответствии с паттерном Абстрактная фабрика. 
Необходимо реализовать приведённые ОО модели, продемонстрировать работу реализованных классов.
*/

using System;

namespace Lesson_07_AbstractFactory_Task_02
{
    enum AppConfig { Mac, Win }
    enum UIConfig { Button, CheckBox }

    interface IButton 
    {
        void Paint() { Console.WriteLine($"{this.GetType().Name} отрисован"); }
    }
    class MacButton : IButton { }
    class WinButton : IButton { }

    interface ICheckBox
    {
        void Paint() { Console.WriteLine($"{this.GetType().Name} отрисован"); }
    }
    class MacCheckBox : ICheckBox { }
    class WinCheckBox : ICheckBox { }

    interface IGUIFactory 
    {
        IButton CreateButton();
        ICheckBox CreateCheckBox();
    }
    class MacFactory : IGUIFactory 
    {
        public IButton CreateButton() 
        {
            return new MacButton();
        }
        public ICheckBox CreateCheckBox() 
        {
            return new MacCheckBox();
        }
    }
    class WinFactory : IGUIFactory 
    {
        public IButton CreateButton()
        {
            return new WinButton();
        }
        public ICheckBox CreateCheckBox()
        {
            return new WinCheckBox();
        }
    }

    class ClienApplication
    {
        IButton Button { get; set; }
        ICheckBox CheckBox { get; set; }
        IGUIFactory GUIFactory { get; set; }

        public ClienApplication(AppConfig config)
        {
            GUIFactory = ApplicationConfigurator(config);
        }
        IGUIFactory ApplicationConfigurator(AppConfig config) 
        {
            if (config == AppConfig.Mac)
                return new MacFactory();
            else if (config == AppConfig.Win)
                return new WinFactory();
            else
                throw new Exception("Error config factory");
        }
        public void CreateUI(UIConfig uiConfig)
        {
            if (uiConfig == UIConfig.Button)
                Button = GUIFactory.CreateButton();
            else if (uiConfig == UIConfig.CheckBox)
                CheckBox = GUIFactory.CreateCheckBox();
        }
        public void PaintUI(UIConfig uiConfig) 
        {
            if (uiConfig == UIConfig.Button)
                Button.Paint();
            else if (uiConfig == UIConfig.CheckBox)
                CheckBox.Paint();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            ClienApplication clienApplication = new ClienApplication(AppConfig.Mac);

            clienApplication.CreateUI(UIConfig.Button);
            clienApplication.PaintUI(UIConfig.Button);

            clienApplication.CreateUI(UIConfig.CheckBox);
            clienApplication.PaintUI(UIConfig.CheckBox);
        }
    }
}
