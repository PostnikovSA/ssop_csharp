﻿/*
Lesson_03_Exceptions_Task_01

Реализовать обработку исключительных ситуаций. 
Варьируя входными данными обеспечить возникновение исключительной ситуации.
Организовать информативный вывод данных о возникшем исключении на экран с предложением продолжить выполнение, 
проигнорировав ошибку, или завершить выполнение программы.

ArgumentException: Значение параметра, переданное в функцию неверно.

ArgumentNullException: Значение параметра, переданного в функцию является пустой ссылкой, 
что недопустимо по смыслу функции или операции.

InvalidCastException: Некорректное преобразование типов данных.

StackOverflowException: Переполнение стека. Возникает, как правило, при неверной рекурсии.

OverflowException: Арифметическое переполнение.

DivideByZeroException: В ходе вычислений возникло деление на ноль.

IndexOutOfRangeException: Индекс массива выходит за пределы диапазона. 
Используется в том же смысле для любых коллекций допускающих индексное обращение.

ArrayTypeMismatchException: Тип сохраняемого в массив значения несовместим с типом элементов массива.

OutOfMemoryException: Недостаточно памяти. Обычно возникает в процессе создания нового объекта оператором new.
*/

using System;
using System.Text;


namespace Lesson_03_Exceptions_Task_01
{
    class Program
    {
        static void StrParseByte(string str) 
        {
            try
            {
                Byte.Parse(str);
                Console.WriteLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetType());
                Console.WriteLine(ex.Message);
                Console.WriteLine();
            }
            
        }
        static void SumRecursion(byte a, byte b) 
        {
            try
            {
                if ((a + b) >= 255)
                    throw new StackOverflowException();
                byte sum = (byte)(a+b);
                SumRecursion(sum, b);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetType());
                Console.WriteLine(ex.Message);
                Console.WriteLine();
            }
        }
        static void Divide(byte a, byte b) 
        {
            try
            {
                byte sum = (byte)(a / b);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetType());
                Console.WriteLine(ex.Message);
                Console.WriteLine();
            }
        }
        static void CreateArray(int length) 
        {
            int[] array = new int[length];
            try
            {
                array[length] = length;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetType());
                Console.WriteLine(ex.Message);
                Console.WriteLine();
            }
        }
        static void CreateObject(object obj, int length) 
        {
            object[] objects = new string[length];
            try
            {
                objects[0] = obj;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetType());
                Console.WriteLine(ex.Message);
                Console.WriteLine();
            }
        }
        static void InsertInString(string str) 
        {
            StringBuilder sb = new StringBuilder(10, 10);
            sb.Append("old string");

            try
            {
                sb.Insert(0, str, 1);
                Console.WriteLine(sb);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetType());
                Console.WriteLine(ex.Message);
                Console.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            StrParseByte("");
            StrParseByte(null);
            StrParseByte("Mike");
            SumRecursion(1, 2);
            StrParseByte("256");
            Divide(2,0);
            CreateArray(2);
            CreateObject(2, 2);
            InsertInString(" ");
        }
    }
}
