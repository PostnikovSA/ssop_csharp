﻿/*
Lesson_05_OverloadingOperations_Task_01

Написать программу на языке C#, в ней должны присутствовать 
как перегруженные операторы, так и пользовательские преобразования между типами данных.

Реализовать класс комплексных чисел, переопределив операции 
сложения, вычитания, умножения, деления и операторы сравнения. 
Учесть случаи операций не только комплексных чисел между собой, но и с вещественными числами.
*/

using System;
using System.Numerics;

namespace Lesson_05_OverloadingOperations_Task_01
{
    class ComplexDigits
    {
        public dynamic FirstDigit { get; }
        public dynamic SecondDigit { get; }

        public ComplexDigits(dynamic firstDigit, dynamic secondDigit)
        {
            if ((firstDigit is int) || (firstDigit is double) && (secondDigit is int) || (secondDigit is double))
            {
                FirstDigit = firstDigit;
                SecondDigit = secondDigit;
            }
            else
                throw new Exception("Error. Insert only Int or Double values");
        }

        public static ComplexDigits operator +(dynamic firstDigit, ComplexDigits complexDigits2)
        {
            return new ComplexDigits
            (firstDigit + complexDigits2.FirstDigit,
            firstDigit + complexDigits2.SecondDigit);
        }
        public static ComplexDigits operator -(dynamic firstDigit, ComplexDigits complexDigits2)
        {
            return new ComplexDigits
            (firstDigit - complexDigits2.FirstDigit,
            firstDigit - complexDigits2.SecondDigit);
        }
        public static ComplexDigits operator *(dynamic firstDigit, ComplexDigits complexDigits2)
        {
            return new ComplexDigits
            (firstDigit * complexDigits2.FirstDigit,
            firstDigit * complexDigits2.SecondDigit);
        }
        public static ComplexDigits operator /(dynamic firstDigit, ComplexDigits complexDigits2)
        {
            return new ComplexDigits
            ((double)firstDigit / (double)complexDigits2.FirstDigit,
            (double)firstDigit / (double)complexDigits2.SecondDigit);
        }
        public static ComplexDigits operator +(ComplexDigits complexDigits1, dynamic secondDigit)
        {
            return new ComplexDigits
            (complexDigits1.FirstDigit + secondDigit,
            complexDigits1.SecondDigit + secondDigit);
        }
        public static ComplexDigits operator -(ComplexDigits complexDigits1, dynamic secondDigit)
        {
            return new ComplexDigits
            (complexDigits1.FirstDigit - secondDigit,
            complexDigits1.SecondDigit - secondDigit);
        }
        public static ComplexDigits operator *(ComplexDigits complexDigits1, dynamic secondDigit)
        {
            return new ComplexDigits
            (complexDigits1.FirstDigit * secondDigit,
            complexDigits1.SecondDigit * secondDigit);
        }
        public static ComplexDigits operator /(ComplexDigits complexDigits1, dynamic secondDigit)
        {
            return new ComplexDigits
            ((double)complexDigits1.FirstDigit / (double)secondDigit,
            (double)complexDigits1.SecondDigit / (double)secondDigit);
        }
        public static ComplexDigits operator +(ComplexDigits complexDigits1, ComplexDigits complexDigits2)
        {
            return new ComplexDigits
            (complexDigits1.FirstDigit + complexDigits2.FirstDigit,
            complexDigits1.SecondDigit + complexDigits2.SecondDigit);
        }
        public static ComplexDigits operator -(ComplexDigits complexDigits1, ComplexDigits complexDigits2) 
        {
            return new ComplexDigits
            (complexDigits1.FirstDigit - complexDigits2.FirstDigit,
            complexDigits1.SecondDigit - complexDigits2.SecondDigit);
        }
        public static ComplexDigits operator *(ComplexDigits complexDigits1, ComplexDigits complexDigits2)
        {
            return new ComplexDigits
            (complexDigits1.FirstDigit * complexDigits2.FirstDigit,
            complexDigits1.SecondDigit * complexDigits2.SecondDigit);
        }
        public static ComplexDigits operator /(ComplexDigits complexDigits1, ComplexDigits complexDigits2)
        {
            return new ComplexDigits
            ((double)complexDigits1.FirstDigit / (double)complexDigits2.FirstDigit,
            (double)complexDigits1.SecondDigit / (double)complexDigits2.SecondDigit);
        }
        public static bool operator ==(dynamic firstDigit, ComplexDigits complexDigits2)
        {
            return
                ((firstDigit == complexDigits2.FirstDigit) ||
                (firstDigit == complexDigits2.SecondDigit));
        }
        public static bool operator !=(dynamic firstDigit, ComplexDigits complexDigits2)
        {
            return
                ((firstDigit != complexDigits2.FirstDigit) ||
                (firstDigit != complexDigits2.SecondDigit));
        }
        public static bool operator >(dynamic firstDigit, ComplexDigits complexDigits2)
        {
            return
                ((firstDigit > complexDigits2.FirstDigit) ||
                (firstDigit > complexDigits2.SecondDigit));
        }
        public static bool operator <(dynamic firstDigit, ComplexDigits complexDigits2)
        {
            return
                ((firstDigit < complexDigits2.FirstDigit) ||
                (firstDigit < complexDigits2.SecondDigit));
        }
        public static bool operator ==(ComplexDigits complexDigits1, dynamic secondDigit)
        {
            return
                ((complexDigits1.FirstDigit == secondDigit) ||
                (complexDigits1.SecondDigit == secondDigit));
        }
        public static bool operator !=(ComplexDigits complexDigits1, dynamic secondDigit)
        {
            return
                ((complexDigits1.FirstDigit != secondDigit) ||
                (complexDigits1.SecondDigit != secondDigit));
        }
        public static bool operator >(ComplexDigits complexDigits1, dynamic secondDigit)
        {
            return
                ((complexDigits1.FirstDigit > secondDigit) ||
                (complexDigits1.SecondDigit > secondDigit));
        }
        public static bool operator <(ComplexDigits complexDigits1, dynamic secondDigit)
        {
            return
                ((complexDigits1.FirstDigit < secondDigit) ||
                (complexDigits1.SecondDigit < secondDigit));
        }
        public static bool operator ==(ComplexDigits complexDigits1, ComplexDigits complexDigits2)
        {
            return
                ((complexDigits1.FirstDigit == complexDigits2.FirstDigit) ||
                (complexDigits1.SecondDigit == complexDigits2.SecondDigit));
        }
        public static bool operator !=(ComplexDigits complexDigits1, ComplexDigits complexDigits2)
        {
            return 
                ((complexDigits1.FirstDigit != complexDigits2.FirstDigit) || 
                (complexDigits1.SecondDigit != complexDigits2.SecondDigit));
        }
        public static bool operator >(ComplexDigits complexDigits1, ComplexDigits complexDigits2)
        {
            return
                ((complexDigits1.FirstDigit > complexDigits2.FirstDigit) ||
                (complexDigits1.SecondDigit > complexDigits2.SecondDigit));
        }
        public static bool operator <(ComplexDigits complexDigits1, ComplexDigits complexDigits2)
        {
            return
                ((complexDigits1.FirstDigit < complexDigits2.FirstDigit) ||
                (complexDigits1.SecondDigit < complexDigits2.SecondDigit));
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            ComplexDigits complexDigits1 = new ComplexDigits(4,9);
            ComplexDigits complexDigits2 = new ComplexDigits(2,3);
        }
    }
}
