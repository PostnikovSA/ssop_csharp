﻿/*
Lesson_02_Arrays_Task_05

Создать программу демонстрирующую работу классов.
Реализовать иерархию классов, содержащих поля, свойства и методы, согласно варианту задания:
принтер, компьютер, компьютерная техника, монитор, винчестер, клавиатура, запоминающее устройство.
*/


using System;
using System.Collections.Generic;

namespace Lesson_02_ClassHierarchy_Task_05
{
    abstract class ComputerTechnology 
    {
        public ComputerTechnology()
        {
            Console.Write("Computer Technology -> ");
        }
    }
    class Monitor : ComputerTechnology
    {
        public Monitor()
        {
            Console.WriteLine("Monitor");
        }
    }
    class Winchester : ComputerTechnology 
    {
        public Winchester()
        {
            Console.WriteLine("Winchester");
        }
    }
    class Keyboard : ComputerTechnology 
    {
        public Keyboard()
        {
            Console.WriteLine("Keyboard");
        }
    }
    class Computer : ComputerTechnology 
    {
        Monitor monitor;
        Winchester winchester;
        Keyboard keyboard;
        public Computer()
        {
            Console.WriteLine("Computer:");
            monitor = new Monitor();
            winchester = new Winchester();
            keyboard = new Keyboard();
        }
    }
    class Printer : ComputerTechnology
    {
        public Printer()
        {
            Console.WriteLine("Printer");
        }
    }
    class MemoryDevice : ComputerTechnology 
    {
        public MemoryDevice()
        {
            Console.WriteLine("MemoryDevice");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Monitor monitor = new Monitor();
            Console.WriteLine();

            Winchester winchester = new Winchester();
            Console.WriteLine();

            Keyboard keyboard = new Keyboard();
            Console.WriteLine();

            Computer computer = new Computer();
            Console.WriteLine();

            Printer printer = new Printer();
            Console.WriteLine();

            MemoryDevice memoryDevice = new MemoryDevice();
            Console.WriteLine();
        }
    }
}
