﻿/*
Lesson_02_Arrays_Task_03

Создать программу демонстрирующую работу классов.
Реализовать иерархию классов, содержащих поля, свойства и методы, согласно варианту задания:
строение, комната, мебель, стул, холодильник, многоэтажка, кухня, лампа.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_02_ClassHierarchy_Task_03
{
    abstract class Building
    {
        public double TotalAreaBuilding { get; set; }
        public int StoreysBuilding { get; set; }
    }
    class HighRiseBuilding : Building
    {
        public int Storey { get; set; }
        public HighRiseBuilding()
        {
            AddHighRiseBuildingInfo();
        }
        public void AddHighRiseBuildingInfo()
        {
            Console.Write("Input number storeys building: ");
            this.StoreysBuilding = int.Parse(Console.ReadLine());
            Console.Write("Input number storey building: ");
            this.Storey = int.Parse(Console.ReadLine());
            Console.Write("Input number total area building: ");
            this.TotalAreaBuilding = double.Parse(Console.ReadLine());
            Console.WriteLine();
        }
    }

    abstract class Rooms
    {
        public double AreaRoom { get; set; }
    }
    class Kitchen : Rooms
    {
        public Kitchen()
        {
            AddKitchenCharacteristics();
        }
        public void AddKitchenCharacteristics()
        {
            Console.Write("Input number area kitchen: ");
            this.AreaRoom = double.Parse(Console.ReadLine());
        }
    }

    abstract class HouseholdItems
    {
        protected int Voltage { get; set; }
    }
    class Lamp : HouseholdItems
    {
        bool _lightOn;
        public Lamp()
        {
            AddLampInfo();
        }
        public void AddLampInfo()
        {
            Console.Write("Input number voltage using lamp: ");
            this.Voltage = int.Parse(Console.ReadLine());

            _lightOn = false;
        }
        public void SwitchLigth()
        {
            if (_lightOn == false)
            {
                _lightOn = true;
                Console.WriteLine("Ligth On\n");
            }
            else
            {
                _lightOn = false;
                Console.WriteLine("Ligth Off\n");
            }
        }
    }
    class Fridge : HouseholdItems
    {
        bool _frostOn;
        public Fridge()
        {
            AddFridgeInfo();
        }
        public void AddFridgeInfo()
        {
            Console.Write("Input number voltage using fridge: ");
            this.Voltage = int.Parse(Console.ReadLine());

            _frostOn = false;
        }
        public void SwitchFrost()
        {
            if (_frostOn == false)
            {
                _frostOn = true;
                Console.WriteLine("Frost On\n");
            }
            else
            {
                _frostOn = false;
                Console.WriteLine("Frost Off\n");
            }
        }
    }

    abstract class Furniture
    {
        protected double Length { get; set; }
        protected double Width { get; set; }
        protected double Height { get; set; }
    }
    class Chair : Furniture 
    {
        public Chair()
        {
            AddChairInfo();
        }
        public void AddChairInfo()
        {
            Console.Write("Input number length chair: ");
            this.Length = int.Parse(Console.ReadLine());

            Console.Write("Input number width chair: ");
            this.Width = int.Parse(Console.ReadLine());

            Console.Write("Input number height chair: ");
            this.Height = int.Parse(Console.ReadLine());
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            HighRiseBuilding building1 = new HighRiseBuilding();

            Lamp lamp = new Lamp();
            lamp.SwitchLigth();
            lamp.SwitchLigth();

            Fridge fridge = new Fridge();
            fridge.SwitchFrost();
            fridge.SwitchFrost();

            Chair chair = new Chair();
        }
    }
}
