﻿/*
Lesson_02_Arrays_Task_02

Создать программу демонстрирующую работу классов.
Реализовать иерархию классов, содержащих поля, свойства и методы, согласно варианту задания:
cредства передвижения, пассажир, автомобиль, поезд, ребенок, самолет.
 */



using Lesson_02_ClassHierarchy_Task_02;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_02_ClassHierarchy_Task_02
{
    abstract class Vehicle
    {
        protected int _maxSpeed;
    }
    interface IMove
    {
        int Move();
    }
    abstract class Pedestrian : Vehicle
    {
        public Pedestrian()
        {
            Console.WriteLine("Pedestrian");
        }
    }
    class Child : Pedestrian, IMove
    {
        public Child()
        {
            Console.WriteLine($"Child\nMax Speed: {Move()} km/h\n");
        }
        public int Move() => _maxSpeed = 3;
    }
    class Adult : Pedestrian, IMove
    {
        public Adult()
        {
            Console.WriteLine($"Adult\nMax Speed: {Move()} km/h\n");
        }
        public int Move() => _maxSpeed = 7;
    }
    abstract class Passanger : Vehicle
    {
        public Passanger()
        {
            Console.WriteLine("Passanger");
        }
    }
    class Car : Passanger, IMove
    {
        public Car()
        {
            Console.WriteLine($"Car\nMax Speed: {Move()} km/h\n");
        }
        public int Move() => _maxSpeed = 90;
    }
    class Train : Passanger, IMove
    {
        public Train()
        {
            Console.WriteLine($"Train\nMax Speed: {Move()} km/h\n");
        }
        public int Move() => _maxSpeed = 160;
    }
    class Jet : Passanger, IMove
    {
        public Jet()
        {
            Console.WriteLine($"Jet\nMax Speed: {Move()} km/h\n");
        }
        public int Move() => _maxSpeed = 600;
    }

    class Program
    {
        static void Main(string[] args)
        {
            Child human_1 = new Child();
            human_1.Move();

            Adult human_2 = new Adult();
            human_2.Move();

            Car human_3 = new Car();
            human_3.Move();

            Train human_4 = new Train();
            human_4.Move();

            Jet human_5 = new Jet();
            human_5.Move();
        }
    }
}


