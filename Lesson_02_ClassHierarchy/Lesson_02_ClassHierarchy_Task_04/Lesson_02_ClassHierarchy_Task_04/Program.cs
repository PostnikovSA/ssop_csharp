﻿/*
Lesson_02_Arrays_Task_04

Создать программу демонстрирующую работу классов.
Реализовать иерархию классов, содержащих поля, свойства и методы, согласно варианту задания:
домашняя утварь, электрическая техника, холодильник, лампа, утюг, механические приборы, ложка, вилка, пылесос.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Lesson_02_ClassHierarchy_Task_04
{
    class HomeStuff
    {
        public HomeStuff()
        {
            Console.WriteLine("Home Stuff");
        }
    }
    class ElectricalEngineering : HomeStuff
    {
        public ElectricalEngineering()
        {
            Console.WriteLine("Electrical Engineering");
        }
    }
    class MechanicalDevices : HomeStuff
    {
        public MechanicalDevices()
        {
            Console.WriteLine("Mechanical Devices");
        }
    }
    class Refrigerator : ElectricalEngineering
    {
        public Refrigerator()
        {
            Console.WriteLine("Refrigerator");
        }
    }
    class Iron : ElectricalEngineering
    {
        public Iron()
        {
            Console.WriteLine("Iron");
        }
    }
    class VacuumCleaner : ElectricalEngineering
    {
        public VacuumCleaner()
        {
            Console.WriteLine("Vacuum Cleaner");
        }
    }
    class Lamp : ElectricalEngineering
    {
        public Lamp()
        {
            Console.WriteLine("Lamp");
        }
    }
    class Fork : MechanicalDevices
    {
        public Fork()
        {
            Console.WriteLine("Fork");
        }
    }
    class Spoon : MechanicalDevices
    {
        public Spoon()
        {
            Console.WriteLine("Spoon");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            VacuumCleaner vacuumCleaner = new VacuumCleaner();
            Console.WriteLine();

            Refrigerator refrigerator = new Refrigerator();
            Console.WriteLine();

            Iron iron = new Iron();
            Console.WriteLine();

            Lamp lamp = new Lamp();
            Console.WriteLine();

            Fork fork = new Fork();
            Console.WriteLine();

            Spoon spoon = new Spoon();
            Console.WriteLine();
        }
    }
}
