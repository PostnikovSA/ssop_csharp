﻿/*
Lesson_02_Arrays_Task_06

Создать программу демонстрирующую работу классов.
Реализовать иерархию классов, содержащих поля, свойства и методы, согласно варианту задания:
человек, сотрудник, рабочий, студент, библиотекарь, директор, сотрудники.
*/


using System;
using System.Collections.Generic;

namespace Lesson_02_ClassHierarchy_Task_06
{
    abstract class Human 
    {
        public Human()
        {
            Console.Write("Human -> ");
        }
    }
    abstract class Employer : Human 
    {
        public Employer()
        {
            Console.Write("Employer -> ");
        }
    }
    class Worker : Employer 
    {
        public Worker()
        {
            Console.WriteLine("Worker");
        }
    }
    class Student : Employer 
    {
        public Student()
        {
            Console.WriteLine("Student");
        }
    }
    class Librarian : Employer 
    {
        public Librarian()
        {
            Console.WriteLine("Librarian");
        }
    }
    class Director : Employer 
    {
        public Director()
        {
            Console.WriteLine("Director");
        }
    }
    class Employers : Human 
    {
        public List<Employer> employers;
        public Employers()
        {
            Console.WriteLine("Employers:");
            employers = new List<Employer>();
        }
        public void PrintList() 
        {
            for (int i = 0; i < employers.Count; i++)
            {
                Console.WriteLine(employers[i]);
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Worker worker = new Worker();
            Console.WriteLine();

            Student student = new Student();
            Console.WriteLine();

            Librarian librarian = new Librarian();
            Console.WriteLine();

            Director director = new Director();
            Console.WriteLine();

            Employers employers = new Employers();
            employers.employers.Add(worker);
            employers.employers.Add(director);
            employers.employers.Add(student);
            employers.employers.Add(librarian);
            employers.PrintList();
            Console.WriteLine();
        }
    }
}
