﻿/*
Lesson_02_Arrays_Task_07

Создать программу демонстрирующую работу классов.
Реализовать иерархию классов, содержащих поля, свойства и методы, согласно варианту задания:
бумага, газета, книга, журнал, учебник, плакат, картина, библиотека, газетный киоск.
*/


using System;
using System.Collections.Generic;

namespace Lesson_02_ClassHierarchy_Task_07
{
    abstract class Location 
    {
        public string Name { get; protected set; }
        public string Info { get; protected set; }
        public List<PaperProduct> PP { get; protected set; }
        public Location()
        {
            Console.Write("Created ");
            PP = new List<PaperProduct>();
        }
        public void CreateStudyBook(string name)
        {
            new StudyBook(name, this);
        }
        public void CreatePicture(string name)
        {
            new Picture(name, this);
        }
        public void CreateNewsPaper(string name)
        {
            new NewsPaper(name, this);
        }
        public void CreateMagazine(string name)
        {
            new Magazine(name, this);
        }
        public void CreatePoster(string name)
        {
            new Poster(name, this);
        }
        public void PrintListInfo()
        {
            Console.WriteLine($"{this.Name} Lists:");
            for (int i = 0; i < PP.Count; i++)
            {
                Console.WriteLine(PP[i].Name);
            }
            Console.WriteLine();
        }
    }
    class Library : Location 
    {
        public Library(string name)
        {
            this.Info = "For Rent";
            this.Name = name;
            Print();
        }
        void Print()
        {
            Console.WriteLine("Library");
            Console.WriteLine($"Name: {this.Name}\n");
        }
    }
    class NewsStand : Location
    {
        public NewsStand(string name)
        {
            this.Name = name;
            this.Info = "For Sell";
            Print();
        }
        void Print() 
        {
            Console.WriteLine("NewsStand");
            Console.WriteLine($"Name: {this.Name}\n");
        }
    }

    abstract class PaperProduct 
    {
        public string Name { get; set; }
        public string Info { get; protected set; }
        public PaperProduct()
        {
            Console.Write("Created ");
        }
    }
    class Picture : PaperProduct 
    {
        public Picture(string name, Location location)
        {
            this.Info = "Drawn";
            this.Name = name;
            location.PP.Add(this);

            Console.WriteLine("Picture ");
            Console.WriteLine($"Name: {this.Name}\n");
        }
    }
    class NewsPaper : PaperProduct
    {
        public NewsPaper(string name, Location location)
        {
            this.Info = "Printed";
            this.Name = name;
            location.PP.Add(this);

            Console.WriteLine("NewsPaper ");
            Console.WriteLine($"Name: {this.Name}\n");
        }
    }
    class Magazine : PaperProduct
    {
        public Magazine(string name, Location location)
        {
            this.Info = "Printed";
            this.Name = name;
            location.PP.Add(this);

            Console.WriteLine("Magazine ");
            Console.WriteLine($"Name: {this.Name}\n");
        }
    }
    class StudyBook : PaperProduct
    {
        public StudyBook(string name, Location location)
        {
            this.Info = "Printed";
            this.Name = name;
            location.PP.Add(this);

            Console.WriteLine("StudyBook ");
            Console.WriteLine($"Name: {this.Name}\n");
        }
    }
    class Poster : PaperProduct 
    {
        public Poster(string name, Location location)
        {
            this.Info = "Printed";
            this.Name = name;
            location.PP.Add(this);

            Console.WriteLine("Poster ");
            Console.WriteLine($"Name: {this.Name}\n");
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            NewsStand molot = new NewsStand("Molot");
            Library publLibr = new Library("Public Library");

            molot.CreateStudyBook("middle step");
            molot.CreatePicture("red horse");
            molot.CreateMagazine("Look Out");

            publLibr.CreateStudyBook("intern step");
            publLibr.CreateNewsPaper("True Lies");
            publLibr.CreatePoster("Are you ready?");

            molot.PrintListInfo();
            publLibr.PrintListInfo();
        }
    }
}
