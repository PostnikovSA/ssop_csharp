﻿/*
Lesson_02_Arrays_Task_01

Создать программу демонстрирующую работу классов.
Реализовать иерархию классов, содержащих поля, свойства и методы, согласно варианту задания:
животное, млекопитающее, лошадь, рыбы, насекомые, пауки.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_02_ClassHierarchy_Task_01
{
    class Animalia
    {
        public Animalia()
        {
            Console.WriteLine("Kingdom: Animalia");
        }
    }

    class Chordata : Animalia
    {
        public Chordata()
        {
            Console.WriteLine("Phylum: Chordata");
        }
    }

    class Arthropods : Animalia
    {
        public Arthropods()
        {
            Console.WriteLine("Phylum: Arthropods");
        }
    }

    class Mammalia : Chordata 
    {
        public Mammalia()
        {
            Console.WriteLine("Class: Mammalia");
        }
    }

    class Fish : Chordata 
    {
        public Fish()
        {
            Console.WriteLine("Class: Fish");
        }
    }

    class Insects : Arthropods
    {
        public Insects()
        {
            Console.WriteLine("Class: Insects");
        }
    }

    class Arachnids : Arthropods
    {
        public Arachnids()
        {
            Console.WriteLine("Class: Arachnids");
        }
    }

    class Spyders : Arachnids 
    {
        public Spyders()
        {
            Console.WriteLine("Order: Spyders");
        }
    }

    class Perissodactyla : Mammalia
    {
        public Perissodactyla()
        {
            Console.WriteLine("Order: Perissodactyla");
        }
    }

    class Equidae : Perissodactyla
    {
        public Equidae()
        {
            Console.WriteLine("Famaly: Equidae");
        }
    }

    class Equus : Equidae 
    {
        public Equus()
        {
            Console.WriteLine("Genus: Equus");
        }
    }

    class E_ferus : Equidae 
    {
        public E_ferus()
        {
            Console.WriteLine("Species: E. ferus");
        }
    }

    class E_f_caballus : E_ferus 
    {
        public E_f_caballus()
        {
            Console.WriteLine("Subspecies: E. f. caballus");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("- Животное -");
            Animalia animalia = new Animalia();
            Console.WriteLine();

            Console.WriteLine("- Млекопитающее -");
            Mammalia mammalia = new Mammalia();
            Console.WriteLine();

            Console.WriteLine("- Лошадь -");
            E_f_caballus e_f_caballus = new E_f_caballus();
            Console.WriteLine();

            Console.WriteLine("- Рыбы -");
            Fish fish = new Fish();
            Console.WriteLine();

            Console.WriteLine("- Насекомые -");
            Insects insects = new Insects();
            Console.WriteLine();

            Console.WriteLine("- Пауки -");
            Spyders spyders = new Spyders();
            Console.WriteLine();
        }
    }
}
