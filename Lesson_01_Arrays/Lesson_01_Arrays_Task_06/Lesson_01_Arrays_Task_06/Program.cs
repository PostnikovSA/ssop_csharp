﻿/*
Lesson_01_Arrays_Task_06

Создать проект консольного приложения на языке C#. 
Используя класс Console для ввода/вывода информации на консоль 
реализовать программу в соответствии с одним из ниже приведённых вариантов задания.
В процессе выполнения работы не использовать стандартные функции сортировки и поиска,
содержащиеся в стандартной библиотеке.NET.

Ввести с консоли массив вещественных чисел, вычислить среднегеометрическое значение каждой строки и
среднеарифметическое значение каждого столбца и вывести полученные результаты на экран.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_01_Arrays_Task_06
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            int minNumInArray = 1;
            int maxNumInArray = 10;
            bool isDigit = false;
            do
            {
                Console.Write($"Input number matrix length from {minNumInArray} to {maxNumInArray}: ");
                string str = Console.ReadLine();
                isDigit = int.TryParse(str, out n);
                if ((n > maxNumInArray) || (n < minNumInArray))
                    isDigit = false;
            }
            while (isDigit == false);
            Console.WriteLine();

            double minNum = 0;
            double maxNum = 10;
            double[,] nums = new double[n,n];
            Random rand = new Random();
            for (int i = 0; i < n; i++)
            {
                double sumRows = 0;
                for (int j = 0; j < n; j++)
                {
                    nums[i, j] = rand.NextDouble() * (maxNum - minNum) + minNum;
                    Console.Write($"{Math.Round(nums[i, j], 2)}\t");
                    sumRows += nums[i, j];
                }
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine(Math.Round((sumRows / n), 2));
                Console.ResetColor();
                Console.WriteLine("\n");               
            }

            for (int j = 0; j < n; j++)
            {
                double sumColumns = 0;
                for (int i = 0; i < n; i++)
                {
                    sumColumns += nums[i, j];
                }
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write($"{Math.Round((sumColumns / n), 2)}\t");
                Console.ResetColor();
            }
            Console.Write("\n\n");
        }
    }
}
