﻿/*
Lesson_01_Arrays_Task_04

Создать проект консольного приложения на языке C#. 
Используя класс Console для ввода/вывода информации на консоль 
реализовать программу в соответствии с одним из ниже приведённых вариантов задания.
В процессе выполнения работы не использовать стандартные функции сортировки и поиска,
содержащиеся в стандартной библиотеке.NET.

Используя массивы, ввести с клавиатуры две прямоугольных матрицы и вывести на экран 
результат суммирования первой из них с транспонированной второй матрицей.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_01_Arrays_Task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            int minNumInArray = 1;
            int maxNumInArray = 5;
            bool isDigit = false;
            do
            {
                Console.Write($"Input number matrix columns length from {minNumInArray} to {maxNumInArray}: ");
                string str = Console.ReadLine();
                isDigit = int.TryParse(str, out n);
                if ((n > maxNumInArray) || (n < minNumInArray))
                    isDigit = false;
            }
            while (isDigit == false);
            Console.WriteLine();


            int minNum = 0;
            int maxNum = 10;
            int[,] matrix1 = new int[n, n];
            int[,] matrix2 = new int[n, n];
            Random rand = new Random();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    matrix1[i, j] = rand.Next(minNum, maxNum);
                    matrix2[j, i] = matrix1[i, j];
                }
            }


            for (int i = 0; i < n; i++) 
            {
                for (int j = 0; j < n; j++) 
                {
                    Console.Write($"{matrix1[i, j]}\t");
                }
                Console.WriteLine("\n");
            }
            Console.WriteLine("\n");


            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.Write($"{matrix2[i, j]}\t");
                }
                Console.WriteLine("\n");
            }
            Console.WriteLine("\n");


            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.Write($"{(matrix1[i, j] + matrix2[i, j])}\t");
                }
                Console.WriteLine("\n");
            }
        }
    }
}
