﻿/*
Lesson_01_Arrays_Task_07

Создать проект консольного приложения на языке C#. 
Используя класс Console для ввода/вывода информации на консоль 
реализовать программу в соответствии с одним из ниже приведённых вариантов задания.
В процессе выполнения работы не использовать стандартные функции сортировки и поиска,
содержащиеся в стандартной библиотеке.NET.

Ввести с консоли массив целых чисел и реализовать бинарный поиск по массиву.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_01_Arrays_Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            int minNum = 1;
            int maxNum = 100;
            int minNumInArray = 1;
            int maxNumInArray = 20;
            bool isDigit = false;
            do
            {
                Console.Write($"Input number array length from {minNumInArray} to {maxNumInArray}: ");
                string str = Console.ReadLine();
                isDigit = int.TryParse(str, out n);
                if ((n > maxNumInArray) || (n < minNumInArray))
                    isDigit = false;
            }
            while (isDigit == false);
            Console.WriteLine();


            int[] nums = new int[n];
            Random rand = new Random();
            for (int i = 0; i < nums.Length; i++)
            {
                nums[i] = rand.Next(minNum, maxNum);
                Console.Write($"{nums[i]}   ");
            }
            Console.Write("\n\n");


            for (int i = 1; i < nums.Length; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    if (nums[i] < nums[j])
                    {
                        int value = nums[j];
                        nums[j] = nums[i];
                        nums[i] = value;
                    }
                }
            }


            for (int i = 0; i < nums.Length; i++)
                Console.Write($"{nums[i]}   ");
            Console.Write("\n\n");


            isDigit = false;
            int key = 0;
            do
            {
                Console.Write($"Input key number from {minNum} to {maxNum}: ");
                string str = Console.ReadLine();
                isDigit = int.TryParse(str, out key);
                if ((n > maxNum) || (n < minNum))
                    isDigit = false;
            }
            while (isDigit == false);
            Console.WriteLine();


            int countBrute = 0;
            bool isMatch = false;
            for (int i = 0; i < nums.Length; i++)
            {
                countBrute++;
                if (nums[i] == key)
                {
                    isMatch = true;
                    break;
                }
            }
            Console.WriteLine($"countBrute: {countBrute}\nisMatch = {isMatch}\n");


            int countBinar = 0;
            isMatch = false;
            int startIndex = 0;
            int endIndex = nums.Length - 1;
            int tempIndex = endIndex;
            while ((startIndex <= endIndex))
            {
                countBinar++;
                tempIndex = (startIndex + endIndex) / 2;

                if (nums[tempIndex] == key)
                {
                    isMatch = true;
                    break;
                }
                else if (nums[tempIndex] < key)
                {
                    startIndex = tempIndex + 1;
                }
                else
                {
                    endIndex = tempIndex - 1;
                }
            }
            Console.WriteLine($"countBinar: {countBinar}\nisMatch = {isMatch}\n");
        }
    }
}
