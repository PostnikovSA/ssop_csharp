﻿/*
Lesson_01_Arrays_Task_03

Создать проект консольного приложения на языке C#. 
Используя класс Console для ввода/вывода информации на консоль 
реализовать программу в соответствии с одним из ниже приведённых вариантов задания.
В процессе выполнения работы не использовать стандартные функции сортировки и поиска,
содержащиеся в стандартной библиотеке.NET.

Ввести с консоли массив целых чисел и отсортировать его методом пузырька.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_01_Arrays_Task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            bool isDigit = false;
            do
            {
                Console.Write("Input number: ");
                string str = Console.ReadLine();
                isDigit = int.TryParse(str, out n);
            }
            while (isDigit == false);
            Console.WriteLine();

            int[] nums = new int[n];
            Random rand = new Random();
            for (int i = 0; i < nums.Length; i++)
            {
                nums[i] = rand.Next(1, 10);
                Console.Write($"{nums[i]}   ");
            }
            Console.Write("\n\n");

            int sizeArray = nums.Length;
            for (int i = 0; i < sizeArray - 1; i++)
            {
                for (int j = 0; j < sizeArray - i - 1; j++)
                {
                    if (nums[j] > nums[j + 1])
                    {
                        int value = nums[j];
                        nums[j] = nums[j + 1];
                        nums[j + 1] = value;
                    }
                }
            }

            for (int i = 0; i < nums.Length; i++)
                Console.Write($"{nums[i]}   ");
            Console.Write("\n\n");




        }
    }
}
