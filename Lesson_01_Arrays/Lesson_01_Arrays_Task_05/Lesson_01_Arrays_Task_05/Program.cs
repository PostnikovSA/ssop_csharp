﻿/*
Lesson_01_Arrays_Task_05

Создать проект консольного приложения на языке C#. 
Используя класс Console для ввода/вывода информации на консоль 
реализовать программу в соответствии с одним из ниже приведённых вариантов задания.
В процессе выполнения работы не использовать стандартные функции сортировки и поиска,
содержащиеся в стандартной библиотеке.NET.

Ввести с клавиатуры массив строк, отсортировать полученный массив по длине строки и вывести результат на экран.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_01_Arrays_Task_05
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            int minNumInArray = 1;
            int maxNumInArray = 10;
            bool isDigit = false;
            do
            {
                Console.Write($"Input number array length from {minNumInArray} to {maxNumInArray}: ");
                string str = Console.ReadLine();
                isDigit = int.TryParse(str, out n);
                if ((n > maxNumInArray) || (n < minNumInArray))
                    isDigit = false;
            }
            while (isDigit == false);
            Console.WriteLine();


            string[] words = new string[n];
            for (int i = 0; i < words.Length; i++)
            {
                Console.Write($"Input word {i + 1}: ");
                words[i] = Console.ReadLine();
            }
            Console.Write("\n\n");


            for (int i = 1; i < words.Length; i++)
            {
                string tempStr;
                for (int j = 0; j < i; j++)
                {
                    if (words[i].Length < words[j].Length) 
                    {
                        tempStr = words[j];
                        words[j] = words[i];
                        words[i] = tempStr;
                    }
                }
            }


            for (int i = 0; i < words.Length; i++)
                Console.WriteLine(words[i]);
            Console.WriteLine();
        }
    }
}
