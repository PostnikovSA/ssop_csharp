﻿/*
Lesson_06_BasicDesignApproaches_Task_02

В презентации 4 - Введение в проектирование (часть 3) на слайде 17 
приведена диаграмма классов ОО моделей, построенных в соответствии с определёнными принципами проектирования. 
Необходимо реализовать приведённые ОО модели, продемонстрировать работу реализованных классов.
*/

using System;
using System.Collections.Generic;

namespace Lesson_06_BasicDesignApproaches_Task_02
{
    interface IEmployee 
    {
        void DoWork();
    }
    abstract class Company
    {
        public virtual List<IEmployee> GetEmployees() 
        {
            return new List<IEmployee>();
        }
        public void CreateSoftware() 
        {
            Console.WriteLine($"{this.GetType().Name}:");
            List<IEmployee> iEmployees = GetEmployees();
            foreach (var item in iEmployees)
            {
                Console.Write($"\t{item.GetType().Name } ");
                item.DoWork();
            }
        }
    }
    class GameDevCompany : Company 
    {
        List<IEmployee> iEmployees { get; set; }
        public GameDevCompany()
        {
            iEmployees = new List<IEmployee>();
        }
        public override List<IEmployee> GetEmployees()
        {
            iEmployees.Add(new Designer());
            iEmployees.Add(new Artist());

            return iEmployees;
        }
    }
    class OutsourcingCompany : Company
    {
        List<IEmployee> iEmployees { get; set; }
        public OutsourcingCompany()
        {
            iEmployees = new List<IEmployee>();
        }
        public override List<IEmployee> GetEmployees()
        {
            iEmployees.Add(new Programmer());
            iEmployees.Add(new Tester());

            return iEmployees;
        }
    }
    class Designer : IEmployee 
    {
        public void DoWork() 
        {
            Console.WriteLine("создаёт креатив");
        }
    }
    class Artist : IEmployee 
    {
        public void DoWork() 
        {
            Console.WriteLine("создаёт модели");
        }
    }
    class Programmer : IEmployee 
    {
        public void DoWork() 
        {
            Console.WriteLine($"пишет код");
        }
    }
    class Tester : IEmployee
    {
        public void DoWork()
        {
            Console.WriteLine($"создаёт тесты");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            GameDevCompany gameDevCompany = new GameDevCompany();
            OutsourcingCompany outsourcingCompany = new OutsourcingCompany();

            gameDevCompany.CreateSoftware();
            Console.WriteLine();
            outsourcingCompany.CreateSoftware();
            Console.WriteLine();
        }
    }
}
