﻿/*
Lesson_06_BasicDesignApproaches_Task_01

В презентации 4 - Введение в проектирование (часть 3) на слайде 11 
приведена диаграмма классов ОО моделей, построенных в соответствии с определёнными принципами проектирования. 
Необходимо реализовать приведённые ОО модели, продемонстрировать работу реализованных классов.
*/

using System;
using System.Collections.Generic;

namespace Lesson_06_BasicDesignApproaches_Task_01
{
    class TaxCalculator
    {
        public double GetTaxRate(string country, Product product) 
        {
            if (country == "US")
                return USTax;
            else if (country == "EU")
                return EUTax;
            else
                return 0;
        }
        double USTax { get { return 0.07; } }
        double EUTax { get { return 0.20; } }        
    }
    class Order 
    {
        string Country { get; set; }
        TaxCalculator taxCalculator;
        List<Product> LineItems { get; set; }
        public Order(string country, List<Product> lineItems)
        {
            Country = country;
            LineItems = lineItems;
            taxCalculator = new TaxCalculator();
        }
        public double GetOrderTotal() 
        {
            double total = 0;
            double subTotal = 0;

            foreach (var item in LineItems)
            {
                subTotal += item.Price * item.Quantity;
                total += subTotal * taxCalculator.GetTaxRate(this.Country, item);
            }
            return total;
        }
    }
    class Product 
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public Product(string name, double price, int quantity)
        {
            Name = name;
            Price = price;
            Quantity = quantity;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<Product> USProducts = new List<Product>();
            USProducts.Add(new Product("Milk", 3.49, 5));
            USProducts.Add(new Product("Water", 1.99, 8));
            Order USOrder = new Order("US", USProducts);
            Console.WriteLine(USOrder.GetOrderTotal());

            List<Product> EUProducts = new List<Product>();
            USProducts.Add(new Product("Milk", 3.49, 5));
            USProducts.Add(new Product("Water", 1.99, 8));
            Order EUOrder = new Order("EU", USProducts);
            Console.WriteLine(USOrder.GetOrderTotal());
        }
    }
}
