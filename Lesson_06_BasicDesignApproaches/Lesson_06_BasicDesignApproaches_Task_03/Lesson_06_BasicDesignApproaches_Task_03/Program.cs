﻿/*
Lesson_06_BasicDesignApproaches_Task_03

В презентации 4 - Введение в проектирование (часть 3) на слайде 22 
приведена диаграмма классов ОО моделей, построенных в соответствии с определёнными принципами проектирования. 
Необходимо реализовать приведённые ОО модели, продемонстрировать работу реализованных классов.
*/

using System;

namespace Lesson_06_BasicDesignApproaches_Task_03
{
    public enum TypeEngine { Combustion, Electric }
    class Transport 
    {
        
        Engine Engine { get; set; }
        Driver Driver { get; set; }
        public Transport(Driver driver, TypeEngine typeEngine)
        {
            Engine = GetEngine(typeEngine);
            Engine.IsInTransport = true;

            Driver = driver;
            Driver.IsInTransport = true;
        }
        Engine GetEngine(TypeEngine typeEngine)
        {
            if (typeEngine == TypeEngine.Combustion)
                return new CombustionEngine();
            else if (typeEngine == TypeEngine.Electric)
                return new ElectricEngine();
            else
                throw new ArgumentException("Error. Transport has no engine");
        }
        public void Deliver(double destination, string cargo) 
        {
            Console.Write("The ");
            Driver.Navigate();
            Console.Write(" a transport with an ");
            Engine.Move();
            Console.WriteLine($"Destination: {destination} Km\nCargo: {cargo}\n");
        }
    }
    
    interface IEngine
    {
        void Move();
    }
    class Engine : IEngine
    {
        public bool IsInTransport { get; set; }
        public void Move() 
        {
            if (IsInTransport == false)
                Console.WriteLine($"An {this.GetType().Name} without transport will not move");
            else
                Console.WriteLine($"{this.GetType().Name}");
        }
    }
    class CombustionEngine : Engine { }
    class ElectricEngine : Engine { }
    
    interface IDriver
    {
        void Navigate();
    }
    abstract class Driver : IDriver
    {
        public bool IsInTransport { get; set; }
        public virtual void Navigate() 
        {
            if (IsInTransport == false)
                Console.WriteLine($"{this.GetType().Name} has no control");
            else
                Console.Write($"{this.GetType().Name} driving");
        }
    }
    class Human : Driver { }
    class Robot : Driver { }

    class Program
    {
        static void Main(string[] args)
        {
            // Create drivers
            //Driver driver = new Driver();     //Error. Class Driver Abstract
            Robot driver1 = new Robot();
            Human driver2 = new Human();

            // Drivers has no transport
            driver1.Navigate();
            driver2.Navigate();
            Console.WriteLine();

            // Create engine, it's useless
            Engine engine = new Engine();
            engine.Move();                      // An Engine without transport will not move
            Console.WriteLine();

            // Create transport

            Transport transport1 = new Transport(driver1, TypeEngine.Electric);
            transport1.Deliver(220, "Milk");  

            Transport transport2 = new Transport(driver2, TypeEngine.Combustion);
            transport2.Deliver(110, "Cocos");
        }
    }
}
